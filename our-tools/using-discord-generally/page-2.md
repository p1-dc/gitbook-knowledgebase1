# Understanding \[P1] Bot

## Overview

### \[P1] Bot's Mission

\[P1] Bot is a custom bot designed to facilitate collaboration and education in \[P1].&#x20;

### Find \[P1] Bot

\[P1] Bot is present in all of our Discords.&#x20;

### Accessing \[P1] Bot

Type / in any of our Discord Servers and choose \[P1] Bot from the menu.&#x20;

![](https://i.imgur.com/XB8QFOo.png)

## Understanding Trust \[T]

Trust \[T] is a currency we use to facilitate the mission of our bot (collaboration and education).&#x20;

