# Discord bots

## Access

Typing / in a Discord Server allows you to access a menu to interact with all bots in that Server:&#x20;

![](https://i.imgur.com/cxFLx17.gif)

### Accessing a particular bot

Hover over the bot's icon to see it's name:

![](https://i.imgur.com/cCNGmhc.png)

Click the bot to see only commands related to that bot:&#x20;

![](https://i.imgur.com/EImZoFz.gif)

## Selecting bot parameters

Once your select a command, push Enter to run it. If the command has parameters then it will show you a menu:&#x20;

<img src="https://i.imgur.com/d9yL9Oz.gif" alt="" data-size="original">

In general you should feel free to experiment with parameters as permission limits will stop you from "breaking" anything.&#x20;

Try every command within a bot's panel to learn more about using a bot.&#x20;



## Getting credit

Use /imagine to create AI art and upload the results to: [https://discord.gg/WNsNjV32Sq](https://discord.gg/WNsNjV32Sq) under #discord-bots.

![](https://i.imgur.com/NQtpW6D.png)

