---
description: >-
  A guide for those who already understand the basics and need to know key
  things about working with [P1] in Discord
---

# Discord essentials for intermediate users

1. ## Always open a voice chat's related text channel when using that voice chat.&#x20;
   1. [More details >>](https://www.p1dc.org/p1-oc-new-members-guide/our-tools/using-discord-generally/absolute-beginners-guide-to-discord#using-voice-channels)

<figure><img src="https://i.imgur.com/z7bNe2E.gif" alt=""><figcaption></figcaption></figure>

2. ## Use our bot in any channel by typing "/"

<figure><img src="https://i.imgur.com/b3b2XBM.gif" alt=""><figcaption></figcaption></figure>

3. ## Avoid using the "offline" status because:

It's the digital equivalent of never combing your hair, it saves you time but it ruins your ability to connect.&#x20;

<figure><img src="https://i.imgur.com/oicGNDB.gif" alt=""><figcaption></figcaption></figure>

Once you go offline, you will be forever offline even when shutting down your PC and turning it back on. It means:

❌ No networking opportunities

❌ You won't get mentors reaching out

❌ When companies ask us for recommendations, no one will remember you

❌ You won't get elected to the team because people trust who they see&#x20;

It  means people think you have left the project.&#x20;









