---
description: >-
  This guide is for people new to Discord in general. Experienced? Read the
  headers on the right to see if there is anything you need to brush up on and
  move on.
---

# 1⃣ Absolute beginner's guide to Discord

### Breathe deeply

In general, Discord is a platform focused on functionality more than usability. Feeling lost or unable to use Discord is normal.

### Different versions of Discord

There are two versions of Discord:

* Desktop [application](https://discord.com/)
* Web version

More features are generally possible on the desktop version so we recommend it.

### _Getting credit_

_If you are doing this lesson to get credit write down 5 things that confuse you about Trello before reading this guide._

## Discord's home page

Clicking the Discord logo at the top left will allow you to see your "Discord Me" page.

<figure><img src="https://i.imgur.com/nefcLbi.png" alt=""><figcaption></figcaption></figure>

On this page you can [add friends](https://i.imgur.com/E7d7CKb.png), [see messages sent to you](https://i.imgur.com/KbECmwr.gif), and see [who has sent you](https://i.imgur.com/OzDexb9.png) a friend request.

### **Message requests**

When receiving your first message from someone, you will see their message hidden under [Message Requests](https://i.imgur.com/vJszqDU.png).

When receiving a message from someone you already know, you will see their face show up in the top left.

![](https://i.imgur.com/RsHUroT.png)

Click their icon to see their message.

## Servers generally

Guilds or "Servers" you have visited are listed on the left side at all times.

![](https://i.imgur.com/9Yvr93N.png)

Think of each Server as a home with rooms in it. Clicking on a home will cause you to enter that home and will replace your view with the rooms of that home.

You can see which Server you are currently viewing by noticing the white line next to the Server in question.

![](https://i.imgur.com/PFc6b3z.png)

### **Server notifications**

If a Server you are in has messages that you have not read, you will see a small white half circle next to it.

![](https://i.imgur.com/BnBivwX.png)

When clicking on the Server you can see which "channel" or room the message originated in by noticing the white dot next to said channel:

![](https://i.imgur.com/XgWf65d.png)

### **Exiting a Server**

You can always **find your way back home** by clicking the Discord logo:

![](https://i.imgur.com/BCD20EJ.png)

## Types of rooms

### **Categories & channels**

In each Server are categories and channels. You can think of each this way:

1. Categories = Floors
2. Channels = Rooms

![](https://i.imgur.com/5ziruaj.png)

In each category you will find a series of channels related to that category.

You can only view one room at a time. When you click on the room, it will highlight itself:

![](https://i.imgur.com/TyvgRgV.png) _I am now in the "#anything-chat" room and not the #welcomes room._

### **Passive vs active channels**

Some channels are for reading information while others are for chatting with others. To know which is which, hover over the message bar and notice the icon that your mouse transforms into. If it's 🚫symbol then you know it's an information-only channel:

![](https://i.imgur.com/H68VoeV.png)&#x20;

_You will also notice the chat bar is greyed out on such channels._

Active channels will allow you to type in the chat bar.

![](https://i.imgur.com/8YtJHX2.png) _Often active channels are in a category that suggests you can chat._

### **Writing a message**

Once you find an "active" channel, you may type a message by simply clicking the chat bar, typing your message and pushing **Enter ↵**.

{% hint style="info" %}
In \[P1], folks are very helpful. Ask questions in any active channel, and they'll assist you, even if it's not the "right" one.
{% endhint %}

### **Channel types**

You will notice different icons on the left of channels:

![](https://i.imgur.com/fXBhLfV.gif)

These indicate different types of channels.

{% hint style="warning" %}
Clicking on a **"Voice" channel** will cause you to join a **live voice** chat with those already in the channel.
{% endhint %}

## **Using voice channels**

### Speaking

When you join a voice channel you are given a series of options:

![](https://i.imgur.com/yzrnSil.gif)

Click undeafen to hear:

![](https://i.imgur.com/aKGF1k3.png)

Click unmute to speak:

![](https://i.imgur.com/NpKKnUa.png)





One key confusion with voice channels is that you can be in a voice channel and a different text channel both at the same time.&#x20;

\#1 shows me in voice chat whereas the fact that I am still clicked into "#💬General-chit-chat" means #2 is showing me the contents of the text chat.&#x20;

<figure><img src="https://i.imgur.com/iZMbI5t.png" alt=""><figcaption></figcaption></figure>

To avoid confusion, it's best to join the voice channel's embedded text channel whenever in a voice channel.&#x20;

<figure><img src="https://i.imgur.com/z7bNe2E.gif" alt=""><figcaption></figcaption></figure>

### Joining a live steam

When a person is sharing their screen a (LIVE) icon will show next to their name. Hover over their name and click "Watch" to view their stream.

![](https://i.imgur.com/1XZ7rAs.gif)

## **Managing your profile**&#x20;

### **Uploading an avatar**

Follow these steps to upload an avatar:

1. Click your avatar in the bottom left:&#x20;

![](https://i.imgur.com/A7ayczw.png)

2. Click the pencil icon:

![](https://i.imgur.com/yIg20dT.png)

3. Click "Change avatar"

![](https://i.imgur.com/YEDZgSf.png)

4. Upload an image.&#x20;

### Changing your name

1. Click user settings

![](https://i.imgur.com/DgCDWEk.png)

2. Click "Edit" next to username

![](https://i.imgur.com/t8PBnqA.png)

3. Put in your new username

<figure><img src="https://i.imgur.com/9YwvxDt.png" alt=""><figcaption></figcaption></figure>

## Special formatting&#x20;

### Tagging people

By typing the @ symbol before typing someone's name you can send them a direct notification of your message, guiding them to a highlighted post with your content:

![](https://i.imgur.com/z7v1730.png)

![](https://i.imgur.com/G3zuUKJ.png)



### Tagging channels

You can also start typing with # to link a channel:

![](https://i.imgur.com/EhxbMxF.gif)

### Time tags

Timestamps which are highlighted are automatically translated into your local time zone:&#x20;

![](https://i.imgur.com/auCIWSE.png)&#x20;

## Inviting people to a Server

Most Discords allow you to invite other people. To do so click the "invite" button next to a channel:

![](https://i.imgur.com/pitgwmG.png)

Make sure to set the invite expiration to "NEVER" to avoid confusion:&#x20;

![](https://i.imgur.com/PhViFik.gif)

Whichever channel you chose to generate the invite on, is the channel they will see first when joining the Server.&#x20;

{% hint style="info" %}
Tip: You can direct people to an exact channel using that same invitation even if they are already in the Server.
{% endhint %}

### Inviting people on mobile

To accept invites on mobile, you need to join Servers using the "Join code" which is this part of the invite link:&#x20;

![](https://i.imgur.com/VYMSOUu.png)

Put that code into the "Add a Server" menu at the bottom left of your Discord Server list.&#x20;

![](https://i.imgur.com/DuLvwty.png)

![](https://i.imgur.com/TfitPiL.png)

![](https://i.imgur.com/i1fH0iK.png)

## Image posts

To upload images to Discord:

1. Find a channel you want to upload to
2. Find the image you want to upload
3. Drag the images to Discord:

![](https://i.imgur.com/SLtiDM6.gif)

## Forum posts

Forum posts differ from regular chat in that it isolates each discussion topic into it's own thread. This is useful for keeping track of a multi-party discussion over a long period of time.&#x20;

### Finding forum channels

Look for the two chat bubbles to identify a forum:&#x20;

![](https://i.imgur.com/h6VaK3z.png)

### Posting to forums

Open the post guidelines and read them before posting:&#x20;

![](https://i.imgur.com/6i3kzOT.png)

Put in a title and click "New Post"&#x20;

![](https://i.imgur.com/Nwqf1kS.png)

(1) Put in your topic's text

(2) Select a tag

(3) Upload an image (optional)

(4) Post.&#x20;

<figure><img src="https://i.imgur.com/cJYiLyR.png" alt=""><figcaption></figcaption></figure>

## Good-to-know tips

Throughout your Discord experience you will encounter popups which will not clearly indicate their closing function.&#x20;

For example, click next to this profile popup to go back to where you were. There should be an (X) button but they just assume you know how to escape popups.

![](https://i.imgur.com/NfN1zIF.png)

### Getting credit for doing this lesson

After this lesson, note how many of your initial Discord questions were answered. Then, think of five new questions that the lesson didn't address.&#x20;

Find your team's Servant Leader in the main Discord server and send them the questions:

![](https://i.imgur.com/kn0k1Dx.gif)

Include that you would like to be sent 50 Trust for completing your Discord lesson.



