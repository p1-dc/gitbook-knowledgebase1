# Earning Trust \[T]

### 📌 Drag the link to this card to your bookmarks

<figure><img src="https://i.imgur.com/0ggyXUP.gif" alt=""><figcaption><p>You will need this guide often!</p></figcaption></figure>

## About Trust

Trust \[T] is a currency we use to facilitate the mission of our bot (collaboration and education).

With large volunteer teams you get people with a variety of skill ranges working in intermittent periods on a larger project. . To ensure the development of the requisite skills and preservation of project coherence, we gate the submission of tasks behind a currency called Trust.

Trust is relatively easy to earn and if you are the habit of:

1. Examining documentation
2. Participating in note-taking
3. Maintaining a clean and organized workspace
4. Conducting peer evaluations of work

Trust will be earned without significant extra effort.

## Earning Trust

### 💬 Being active in Discord

1. Be in **🔊| 💻Work voice chat**: [https://discord.gg/6yyhbRF8Jy](https://discord.gg/6yyhbRF8Jy)
   1. AFK **2 Trust \[T]** per hour
   2. Speaking/Active **20 \[T]** per hour
2. Post your **progress** in **#👀showcase** or **#👀showcase-private**
   1. **20 \[T]** per **image** post
   2. Max 1 per 20 min
3. Sign up to help others in **#💻❓code-questions**
   1. Sign up to be a **helper**: [https://i.imgur.com/eQUDLqY.png](https://i.imgur.com/eQUDLqY.png) **40 \[T]**
   2. **10 \[T]** per helpful **post**
   3. 1/20 min
4. Post in **#👋introduce-yourself**
   1. **20 \[T]**
   2. Once.
5. Using **#daily-check-ins** in [https://trello.com/c/7qJb8nOA](https://trello.com/c/7qJb8nOA) [https://i.imgur.com/y0UQ2bB.png](https://i.imgur.com/y0UQ2bB.png)
   1. **15 \[T]**.

Automated rewards are in BETA! Things may go wrong.

### 📚 Reading documentation

1. Read our Design docs[https://trello.com/c/XUSVKZKJ](https://trello.com/c/XUSVKZKJ)
   1. **50 \[T]**
2. Updating docs [https://trello.com/c/vX52LdOB](https://trello.com/c/vX52LdOB)
   1. **200 \[T]**
3. Doing exams [https://trello.com/c/LX43HVUc](https://trello.com/c/LX43HVUc)
   1. Up to **170 \[T]**
4. Reading academy articles [https://trello.com/c/s55oDqNg](https://trello.com/c/s55oDqNg)
   1. **160 \[T]**
5. Learning how to use our tools (Miro, Blender, Unity) [https://trello.com/c/r87r2cNp](https://trello.com/c/r87r2cNp)
   1. Up to **170 \[T]**.
6. Reading meeting notes [https://trello.com/c/Dn0cOKkZ](https://trello.com/c/Dn0cOKkZ)
   1. **20 \[T]**
7. Making meeting notes [https://trello.com/c/PbzdJjeY](https://trello.com/c/PbzdJjeY)
   1. **50 \[T]**

### 👮‍♀️ Doing essential tasks

1. Reviewing other people’s tasks [https://trello.com/c/rHxn5yVZ](https://trello.com/c/rHxn5yVZ)
   1. **25 \[T] each**
2. Giving interviews [https://trello.com/c/x3fcn8kR](https://trello.com/c/x3fcn8kR)
   1. **200 \[T]** per interview session
3. Making tasks for your team
4. Cleaning up the Trello board, doing Apprenticeship interviews [https://trello.com/c/dKMNtbAr](https://trello.com/c/dKMNtbAr)
   1. **170 \[T] each**
5. Doing scrum meetings [https://trello.com/c/GWwkrIbE](https://trello.com/c/GWwkrIbE)
   1. **200 \[T]**.

### 🎓 Doing Academy exams

1. [https://trello.com/c/LX43HVUc](https://trello.com/c/LX43HVUc)
   1. Up to **170 \[T]**.
