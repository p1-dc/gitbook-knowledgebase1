# Using Trello in \[P1]

## How \[P1] Uses Trello

\[P1] uses Trello as a Kanban board as part of the Scrumban process.

In simple terms, \[P1] organizes tasks on them. _\[P1] also occasionally uses Trello for documentation._

## \[P1] Trellos

\[P1] uses 9 different Trello boards but you only really need to know about 3:

* **Action Trello** — Task organization
* **Member Management** — A place to organize our member’s active status
* **\[P1-A] Academy** — Useful tutorials on on completing tasks.

## Accessing Boards

You will never need to request access to a board. Rather, you will be given access to a Trello via an automated process when appropriate.

## Understanding subcategorization

In Trello, \[P1] uses empty space to signify the beginning/end of a category:

<figure><img src="https://i.imgur.com/WrQTL9Y.gif" alt=""><figcaption></figcaption></figure>

You will also notice <mark style="background-color:green;">Green</mark> start cards and <mark style="background-color:red;">Red</mark> end cards for each category.

## Copying cards

You will often be required to move cards from a public board to a private board where you have full access.

Whenever requested to do this, be sure to copy the card to the appropriate list within the board.

This video will show you how to move cards from one Trello to another:

{% embed url="https://youtu.be/n8CFcgAtwGc" %}

1. Find the card
2. Click copy
3. Choose a Trello
4. Choose the right list
5. Change the name \[If needed]

### List headers

## Creating tasks

## What not to do

Never sort using this button.&#x20;

<figure><img src="https://i.imgur.com/LhPb6zr.png" alt=""><figcaption><p>This messes up the entire sorting of all our cards</p></figcaption></figure>

Never move or comment on template or informational cards. Comment on your own cards or edit copies you make.&#x20;

<figure><img src="https://i.imgur.com/1O79tms.png" alt=""><figcaption><p>Create a new card and start working, don't work in the template.</p></figcaption></figure>

## Moving cards to the right header

How P1 uses labels

