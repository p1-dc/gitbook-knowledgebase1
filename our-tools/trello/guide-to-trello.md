---
description: This guide covers how to use Trello generally and with [P1] specifically.
---

# Guide to using Trello

### _Getting credit_

_If you are doing this lesson to get credit write down 5 things that confuse you about Trello before reading this guide._

## Trello home

Once you have made your Trello account, you can find your Trello homepage just by going to [https://trello.com/](https://trello.com/)

We suggest bookmarking the Trello page:&#x20;

![](https://i.imgur.com/LJmxBUy.gif)

As a \[P1] member you will be given access to a lot of Trello boards but for this tutorial let's use:&#x20;

[https://trello.com/b/D38njtGx/p1-academy](https://trello.com/b/D38njtGx/p1-academy)

Visiting that link will populate your home page with an example Trello board.

### Understanding the home page

Return to Trello.com and see:

![](https://i.imgur.com/PGqum9B.png)

\[X] out the templates for clarity.&#x20;

![](https://i.imgur.com/e3MnfQO.png)

Create a list of recent, relevant boards. Hover over P1 Academy and click ⭐.

![](https://i.imgur.com/S4YUYxF.png)

## Understanding Trello board components

Trello is a task organization system.&#x20;

You can break down the system into 4 levels:&#x20;

{% tabs %}
{% tab title="1. Workspaces" %}
**Groups of boards.**&#x20;

In most cases in \[P1] these are **ignorable**. &#x20;

![](https://i.imgur.com/pFN6qK9.png)
{% endtab %}

{% tab title="2. Boards" %}
**Groups of lists.**

![](https://i.imgur.com/JvirLtU.png)
{% endtab %}

{% tab title="3. Lists" %}
**Groups of tasks.**

![](https://i.imgur.com/zyCfn2w.png)
{% endtab %}

{% tab title="4. Cards" %}
**Tasks.**

![](https://i.imgur.com/fcB4KH1.png)
{% endtab %}
{% endtabs %}

## Working with Boards

Boards can be \[Public] or \[Private].&#x20;

### \[Public] boards

Public boards allow anyone to **see** cards on that board.&#x20;

You still need to be **Invited** to change anything. However, you can always **copy** items from a public board to one you control.&#x20;

### \[Private] boards

See = Invite only

Change = Invite only

## Navigating a Board

### Scrolling sideways

{% tabs %}
{% tab title="On desktop" %}
Hold **Shift ⇧** and roll the mouse wheel to go left and right:&#x20;

![](https://i.imgur.com/PW9yaIH.gif)


{% endtab %}

{% tab title="Manually" %}
Grab the **scrollbar** at the bottom and pull it **sideways**:\
![](https://i.imgur.com/SzUopBJ.gif)
{% endtab %}

{% tab title="On laptop" %}
Place 2 figures on your trackpad "flat mouse" and scroll right:

<img src="https://i.imgur.com/rPbew8n.png" alt="" data-size="line">

![](https://i.imgur.com/f40ebQC.gif)
{% endtab %}
{% endtabs %}

{% hint style="danger" %}
Don't click and drag across as you will move cards by mistake:

![](https://i.gyazo.com/d96c8b12fe41ee51a7a3a2cb45757d7c.gif)
{% endhint %}

## Working with Lists

### What is a list

Lists are a group of cards.

![](https://i.imgur.com/4gp0FPc.png)

### Navigating a list

Grab the navigation bar to view the whole list.

![](https://i.imgur.com/raUYekn.gif)

### Organizing cards using lists

Both cards and lists can be moved simply by dragging them.

![](https://i.imgur.com/dQd9CQy.gif)

![](https://i.imgur.com/dnN3gbv.gif)

### Moving permissions

Everyone that has EDIT permissions on a board can move **lists** and **cards**.

![](https://i.imgur.com/EBz3fU6.png)

## Working with cards

### Types of cards

You can see the **type** of card by noticing the **icons** attached to it.

Read the card types and notice the **symbols** on each:

📰 = Has text inside

📎 = Has attachments

![](https://i.imgur.com/kaZCRWu.png)

Sometimes cards are used as **headers**.&#x20;

![](https://i.imgur.com/g0aLAKd.png)

Sometimes header-cards **contain** useful **information**:

![](https://i.imgur.com/RQatkFS.png)

#### Image cards

Cards will sometimes be used to convey an image:

![](https://i.imgur.com/0hvSTJB.png)

Sometimes image cards can be clicked to reveal interesting information:

![](https://i.imgur.com/NwMGJck.gif)



### Add a card

Anyone with edit permissions can add a card.

![](https://i.imgur.com/jh5XB0k.gif)

1. Click "Add a card."
2. Name your card.
3. Click "Add card."

### Edit a card

Anyone with edit permissions can edit cards even if they did not create them.&#x20;

1. Click on the card.
2. Change the card name or description.
3. Click "Save."

![](https://i.imgur.com/343yT2r.gif)

### Search cards

You can search for cards in the top right corner.

<figure><img src="https://i.imgur.com/vrGPMsW.gif" alt=""><figcaption></figcaption></figure>

{% hint style="danger" %}
The search function in Trello is slow and confusing.

We recommend using Ctrl + F and searching the board that way instead.&#x20;

![](https://i.imgur.com/EY0aFxa.png)
{% endhint %}

### Move a card

1. Click and hold the card.
2. Drag the card to a new place or list.

![](https://i.imgur.com/0cuwQwX.gif)

### Archiving generally

Archiving a task removes it from public view. The task is still visible when using the search function.

#### Archive vs delete

**Archive** = Hide

**Delete** = Totally **delete** card

You can **find archived cards** using **search**.&#x20;

<figure><img src="https://i.imgur.com/vrGPMsW.gif" alt=""><figcaption></figcaption></figure>

#### Archiving a card

Anyone with edit permission can archive a task.&#x20;

1. Click on the card.
2. Click "Archive" at the bottom.

<figure><img src="https://i.imgur.com/UbjCQrF.gif" alt=""><figcaption></figcaption></figure>

### Unarchiving cards

Click "Send to board".&#x20;

![](https://i.imgur.com/jrqLp0j.png)

### Deleting cards

To delete a card you must first archive it:&#x20;

![](https://i.imgur.com/58t8Sea.gif)

### Sharing cards

To share a card click the "Share" button:

![](https://i.imgur.com/cCRJweX.gif)



## Adding and Managing Members

### Add a member to a card

1. Click on a card.
2. Click "Members."
3. Pick a member or add a new one.

<figure><img src="https://i.imgur.com/akoU4wi.gif" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
Quickly add yourself to an open card using **spacebar**.
{% endhint %}

### Remove a member from a card

1. Click on a card.
2. Click the member's avatar.
3. Click "Remove."

![](https://i.imgur.com/NEZgdJC.gif)

## Using Labels and Due Dates

### Add a label

1. Click on a card.
2. Click "Labels."
3. Pick a label or make a new one.

<figure><img src="https://i.imgur.com/5lwwh7p.gif" alt=""><figcaption></figcaption></figure>

### Add a due date

1. Click on a card.
2. Click "Due Date."
3. Pick a date and time.

![](https://i.imgur.com/CwX6oOC.gif)

{% hint style="warning" %}
We at \[P1] never fill in a **time** only a **date**.
{% endhint %}

## Filtering cards

### Seeing only your own cards

Push Q -- and it will stop you seeing other cards!

![](https://i.imgur.com/b3uLkVa.gif)

{% hint style="danger" %}
Be aware -- it's not recommended to move cards while in a filter as you might place them above a header you did not intend to as you did not see it.
{% endhint %}

### Filter cards

1. Click "Filter"
2. Type keywords
3. Or select a label like "Art".

<figure><img src="https://i.imgur.com/kzyljAB.gif" alt=""><figcaption></figcaption></figure>

### Copy a card

1. Click on a card.
2. Click "Copy."
3. Name the copy.
4. Choose a list.
5. Click "Create Card."

[Visual details >>](https://www.p1dc.org/p1-oc-new-members-guide/our-tools/trello/using-trello-in-p1#copying-cards)

## Using Keyboard Shortcuts

### Shortcuts

1. Press "?" on the board.
2. See the list of shortcuts.
3. Use shortcuts to save time.

![](https://i.imgur.com/fMLbK0Q.png)

## Viewing Board Activity

### Activity

1. Click "..."
2. See recent board actions.

![](https://i.imgur.com/upQGJ1s.gif)

{% hint style="danger" %}
This can be very helpful when seeking to find a card you worked on but did not join.
{% endhint %}

### Getting credit for doing this lesson

Note how many of your initial questions (see top) this guide answered. Then, think of five new questions that the lesson didn't address.&#x20;

Type them out and [take a screenshot](https://trello.com/c/DtPIftfc) of what you typed.

Upload that screenshot to the [channel of the same name](https://i.imgur.com/nZ3AOhi.png) in Discord.&#x20;
