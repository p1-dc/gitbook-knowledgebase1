# Table of contents

## ⚒ Our tools

* [💬 Discord](README.md)
  * [👶 Absolute beginner's guide to Discord](our-tools/discord/absolute-beginners-guide-to-discord.md)
  * [👨💼 Essentials for \[P1\] members](our-tools/using-discord-generally/discord-essentials-for-intermediate-users.md)
  * [🤖 Discord bots](our-tools/using-discord-generally/trust.md)
    * [🤔 Understanding \[P1\] Bot](our-tools/using-discord-generally/page-2.md)
      * [🤝 Earning Trust \[T\]](our-tools/discord/discord-bots/understanding-p1-bot/earning-trust.md)
      * [🔍 How to /review tasks](our-tools/discord/discord-bots/understanding-p1-bot/how-to-review-tasks.md)
  * [📅 Timezone Translation & Hammertime](our-tools/using-discord-generally/using-hammertime.md)
  * [📥 Asset Discord](our-tools/using-discord-generally/asset-discord.md)
* [Trello](our-tools/trello/README.md)
  * [Guide to using Trello](our-tools/trello/guide-to-trello.md)
  * [Using Trello in \[P1\]](our-tools/trello/using-trello-in-p1.md)
  * [Add to trello guide](our-tools/trello/add-to-trello-guide.md)
  * [Getting Tasks Reviewed](our-tools/trello/getting-tasks-reviewed.md)
  * [Team Trellos](our-tools/trello/team-trellos.md)
* [Member Management Trello](our-tools/member-management-trello.md)
* [Using Miro](our-tools/using-miro.md)
* [Using Git](our-tools/using-git.md)

## 🎓 Ranks and roles

* [Trust](ranks-and-roles/trust.md)
* [Trail Member -> Elected Member](ranks-and-roles/trail-member-greater-than-elected-member.md)
* [The election process](ranks-and-roles/the-election-process.md)

## 👨🏫 Systems & processes

* [How to make a task](systems-and-processes/how-to-make-a-task.md)
* [Negative feedback policy](systems-and-processes/negative-feedback-policy.md)
* [Gossip](systems-and-processes/gossip.md)
* [Hammertime cerimony](systems-and-processes/hammertime-cerimony.md)
* [Programming documentation](systems-and-processes/programming-documentation.md)

## 💼 Meetings

* [Daily Scrum](meetings/daily-scrum.md)
* [General Planning Meeting](meetings/general-planning-meeting.md)
* [Demo Day](meetings/demo-day.md)
* [Show-and-tell](meetings/show-and-tell.md)
* [Discovering team event times](meetings/discovering-team-event-times.md)

## 💚 Values

* [Servanthood](values/servanthood.md)
* [Page 1](values/page-1.md)

## 📕 Product documentation

* [Design documentation](product-documentation/design-documentation.md)
* [Technical Design Documentation](product-documentation/technical-design-documentation.md)
