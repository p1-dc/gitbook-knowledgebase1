---
description: >-
  In general, Discord is a platform focused on functionality more than
  usability. Feeling lost or unable to use Discord is normal.
---

# Using Discord Generally

